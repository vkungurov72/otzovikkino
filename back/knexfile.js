// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
// eslint-disable-next-line import/no-anonymous-default-export
export default {

  development: {
    client: 'postgresql',
    connection: {
      database: 'domkino',
      user: 'postgres',
      password: 'postgres'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
};