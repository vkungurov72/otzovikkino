/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
    return knex.schema.createTable('ratingfilms',
        function (table) {
            table.integer('film_id').references('id').inTable('films');
            table.integer('rating').notNullable();
            table.integer('user_id').references('id').inTable('users');
        }
    )
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
    return knex.schema.dropTable('ratingfilms')
};


