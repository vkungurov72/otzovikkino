/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
    return knex.schema.createTable('actorsfilms',
        function (table) {
            table.integer('film_id').references('id').inTable('films');
            table.integer('actor_id').references('id').inTable('actors');
        }
    )
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
    return knex.schema.dropTable('actorsfilms')
};

