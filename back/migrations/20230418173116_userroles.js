/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
    return knex.schema.createTable('userroles',
        function (table) {
            table.integer('user_id').references('id').inTable('users');
            table.integer('role_id').references('id').inTable('roles');
        }
    )
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
    return knex.schema.dropTable('userroles')
};