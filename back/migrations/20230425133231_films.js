/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
    return knex.schema.createTable('films',
        function (table) {
            table.increments('id').unique();
            table.string('name');
            table.string('description');
            table.timestamp('daterelease');
            table.string('rating');
            table.integer('budget');
            table.integer('genre_id').references('id').inTable('genres');
        }
    )
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
    return knex.schema.dropTable('films')
};
