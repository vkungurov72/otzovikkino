/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
    return knex.schema.createTable('roles',
        function (table) {
            table.increments('id');
            table.string('role').unique();
        }
    )
};

export const down = function (knex) {
    return knex.schema.dropTable('roles')
};
