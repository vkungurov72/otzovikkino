/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('actors').del()
  await knex('actors').insert([
    {
      firstname: "Джонни",
      lastname: "Депп",
      birthday: "1963-06-09T00:00:00.000"
    },
    {
      firstname: "Элайджа",
      lastname: "Вуд",
      birthday: "1981-01-28T00:00:00.000"
    },
    {
      firstname: "Аль",
      lastname: "Пачино",
      birthday: "1940-04-25T00:00:00.000"
    },
    {
      firstname: "Киану",
      lastname: "Ривз",
      birthday: "1964-09-02T00:00:00.000"
    },
    {
      firstname: "Том",
      lastname: "Хэнкс",
      birthday: "1956-06-09T00:00:00.000"
    },
    {
      firstname: "Кристиан",
      lastname: "Бэйл",
      birthday: "1974-01-30T00:00:00.000"
    },
    {
      firstname: "Лиам",
      lastname: "Нисон",
      birthday: "1952-06-07T00:00:00.000"
    },
    {
      firstname: "Ума",
      lastname: "Турман",
      birthday: "1970-04-29T00:00:00.000"
    },
    {
      firstname: "Тим",
      lastname: "Роббинс",
      birthday: "1958-10-16T00:00:00.000"
    },
  ]);
};