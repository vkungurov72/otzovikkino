/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('userinfo').del()
  await knex('userinfo').insert([
    { user_id: 1, email: 'admin@gmail.com' },
    { user_id: 2, email: 'user@gmail.com' },
    { user_id: 3, email: 'moderator@gmail.com' },
    { user_id: 4, email: 'superuser@gmail.com' },

  ]);
};
