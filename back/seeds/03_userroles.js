/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('userroles').del()
  await knex('userroles').insert([
    { user_id: 1, role_id: 1 },
    { user_id: 2, role_id: 2 },
    { user_id: 3, role_id: 3 },
    { user_id: 4, role_id: 4 },

  ]);
};
