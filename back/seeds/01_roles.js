/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('roles').del()
  await knex('roles').insert([
    { role: 'admin' },
    { role: 'user' },
    { role: 'moderator' },
    { role: 'superuser' }
  ]);
};
