/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('users').del()
  await knex('users').insert([
    { login: 'admin', password: 'admin' },
    { login: 'user', password: 'user' },
    { login: 'moderator', password: 'moderator' },
    { login: 'superuser', password: 'superuser' },

  ]);
};

