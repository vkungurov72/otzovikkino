/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
export const seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('genres').del()
  await knex('genres').insert([
    { name: 'драма' },
    { name: 'комедия' },
    { name: 'боевик' },
    { name: 'триллер' },
    { name: 'ужасы' },
    { name: 'фантастика' },
    { name: 'фэнтези' },
    { name: 'мелодрама' },
    { name: 'приключения' },
    { name: 'детектив' },
    { name: 'исторический' },
    { name: 'вестерн' },
    { name: 'мультфильм' },
    { name: 'музыкальный' },
    { name: 'спортивный' },
    { name: 'документальный' }
  ]);
};
