import UsersService from "../services/UsersService.js";

class UsersController {
    async getAllUsers(req, res) {
        try {
            const users = await UsersService.getAll()
            res.status(200).json(users)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения пользователей" })
        }
    }

    async getUserById(req, res) {
        const id = req.params.id
        try {
            const user = await UsersService.getById(id)
            if (!user) {
                return res.status(404).json({ message: "Пользователь не найден" })
            }
            res.status(200).json(user)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения пользователя" })
        }
    }

    async createUser(req, res) {
        const userData = req.body
        try {
            const newUser = await UsersService.create(userData)
            res.status(201).json(newUser)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка при создании пользователя" })
        }
    }

    async updateUser(req, res) {
        const id = req.params.id
        const userData = req.body
        try {
            const updatedUser = await UsersService.update(id, userData)
            if (!updatedUser) {
                return res.status(404).json({ message: "Пользователь не найден" })
            }
            res.status(200).json(updatedUser)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка обновления пользователя" })
        }
    }

    async deleteUser(req, res) {
        const id = req.params.id
        try {
            const deletedUser = await UsersService.delete(id)
            if (!deletedUser) {
                return res.status(404).json({ message: "Пользователь не найден" })
            }
            res.status(200).json(deletedUser)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка удаления пользователя" })
        }
    }
}
// Экспортируем экземпляр контроллера для использования в других файлах
// eslint-disable-next-line import/no-anonymous-default-export
export default new UsersController()