import GenresService from "../services/GenresService.js";

class GenresController {
    async getAllGenres(req, res) {
        try {
            const genres = await GenresService.getAll()
            res.status(200).json(genres)
        } catch (e) {
            console.log(e)
            res.status(500).send("Внутренняя ошибка сервера")
        }
    }
    async getGenreById(req, res) {
        const id = req.params.id
        try {
            const genre = await GenresService.getById(id)
            if (!genre) {
                res.status(404).json({ message: "Жанр не найден" })
            }
            res.status(200).json(genre)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения жанра" })
        }
    }

    async createGenre(req, res) {
        const genre = req.body
        try {
            const newGenre = await GenresService.create(genre)
            res.status(201).json(newGenre)
        } catch (e) {
            console.log(e)
            res.status(500).send("Внутренняя ошибка сервера")
        }
    }

    async editGenreById(req, res) {
        try {
            const id = req.params.id
            const genre = req.body
            const updatedGenre = await GenresService.updateById(id, genre)
            if (updatedGenre > 0) {
                res.status(200).json({ message: "Жанр успешно обновлен" })
            } else {
                res.status(404).json({ message: "Жанр не найден" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).send("Внутренняя ошибка сервера")
        }
    }

    async deleteGenreById(req, res) {
        try {
            const id = req.params.id
            const deletedGenre = await GenresService.deleteById(id)
            if (deletedGenre > 0) {
                res.status(200).json({ message: "Жанр успешно удален" })
            } else {
                res.status(404).json({ message: "Жанр не найден" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).send("Внутренняя ошибка сервера")
        }
    }
}

export default new GenresController()