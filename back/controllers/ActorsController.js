import ActorsService from "../services/ActorsService.js"

class ActorsController {
    async getAllActors(req, res) {
        try {
            const actors = await ActorsService.getAll()
            res.status(200).json(actors)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения актеров" })
        }
    }

    async getActorById(req, res) {
        try {
            const actor = await ActorsService.getById(req.params.id)
            if (!actor) {
                res.status(404).json({ message: "Актер не найден" })
            }
            res.status(200).json(actor)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения актера" })
        }
    }
    async createActor(req, res) {
        try {
            const actor = await ActorsService.create(req.body)
            res.status(201).json(actor)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка создания актера" })
        }
    }
    async updateActor(req, res) {
        try {
            const updatedActor = await ActorsService.update(req.params.id, req.body)
            if (!updatedActor) {
                res.status(404).json({ message: "Актер не найден" })
            }
            res.status(200).json(updatedActor)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка обновления актера" })
        }
    }
    async deleteActor(req, res) {
        try {
            const deletedActor = await ActorsService.delete(req.params.id)
            if (!deletedActor) {
                res.status(404).json({ message: "Актер не найден" })
            }
            res.status(200).json(deletedActor)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка удаления актера" })
        }
    }
}

// Экспортируем экземпляр контроллера для использования в других файлах
// eslint-disable-next-line import/no-anonymous-default-export
export default new ActorsController()