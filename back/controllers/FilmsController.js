import FilmsService from "../services/FilmsService.js";

class FilmsController {
    async getAllFilms(req, res) {
        try {
            const films = await FilmsService.getAll()
            res.status(200).json(films)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения фильмов" })
        }
    }

    async getFilmById(req, res) {
        const id = req.params.id
        try {
            const film = await FilmsService.getById(id)
            if (!film) {
                return res.status(404).json({ message: "Фильм не найден" })
            }
            res.status(200).json(film)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка получения фильма" })
        }
    }

    async createFilm(req, res) {
        const filmData = req.body
        try {
            const newFilm = await FilmsService.create(filmData)
            res.status(201).json(newFilm)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка создания фильма" })
        }
    }

    async updateFilm(req, res) {
        const id = req.params.id
        const filmData = req.body
        try {
            const updatedFilm = await FilmsService.update(id, filmData)
            if (!updatedFilm) {
                return res.status(404).json({ message: "Фильм не найден" })
            }
            res.status(200).json(updatedFilm)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка обновления фильма" })
        }
    }

    async deleteFilm(req, res) {
        const id = req.params.id
        try {
            const deletedFilm = await FilmsService.delete(id)
            if (!deletedFilm) {
                return res.status(404).json({ message: "Фильм не найден" })
            }
            res.status(200).json(deletedFilm)
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ message: "Ошибка удаления фильма" })
        }
    }
}

// Экспортируем экземпляр контроллера для использования в других файлах
// eslint-disable-next-line import/no-anonymous-default-export
export default new FilmsController()