
import Router from 'express'

import ActorsController from '../controllers/ActorsController.js';

const ActorsRouter = new Router();

ActorsRouter.get('/actors', ActorsController.getAllActors);
ActorsRouter.get('/actors/:id', ActorsController.getActorById);
ActorsRouter.post('/actors', ActorsController.createActor);
ActorsRouter.put('/actors/:id', ActorsController.updateActor);
ActorsRouter.delete('/actors/:id', ActorsController.deleteActor);

export default ActorsRouter