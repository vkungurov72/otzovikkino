
import Router from 'express'
import GenresController from '../controllers/GenresController.js';



const GenresRouter = new Router();

GenresRouter.get('/genres', GenresController.getAllGenres);
GenresRouter.get('/genres/:id', GenresController.getGenreById);
GenresRouter.post('/genres', GenresController.createGenre);
GenresRouter.put('/genres/:id', GenresController.editGenreById);
GenresRouter.delete('/genres/:id', GenresController.deleteGenreById);

export default GenresRouter

/*
import Router from 'express';
import GenresController from '../controllers/GenresController.js';

const GenresRouter = new Router();

GenresRouter.get('/genres', GenresController.getAllGenres);
GenresRouter.get('/genres/:id', GenresController.getGenreById);
GenresRouter.post('/genres', GenresController.createGenre);
GenresRouter.put('/genres/:id', GenresController.editGenreById);
GenresRouter.delete('/genres/:id', GenresController.deleteGenreById);

export default GenresRouter;

*/