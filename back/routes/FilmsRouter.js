
import Router from 'express'

import FilmsController from '../controllers/FilmsController.js';

const FilmsRouter = new Router();

FilmsRouter.get('/films', FilmsController.getAllFilms);
FilmsRouter.get('/films/:id', FilmsController.getFilmById);
FilmsRouter.post('/films', FilmsController.createFilm);
FilmsRouter.put('/films/:id', FilmsController.updateFilm);
FilmsRouter.delete('/films/:id', FilmsController.deleteFilm);

export default FilmsRouter