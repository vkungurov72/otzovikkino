
import Router from 'express';

import UsersController from '../controllers/UsersController.js';

const UsersRouter = new Router();

UsersRouter.get('/users', UsersController.getAllUsers);
UsersRouter.get('/users/:id', UsersController.getUserById);
UsersRouter.post('/users', UsersController.createUser);
UsersRouter.put('/users/:id', UsersController.updateUser);
UsersRouter.delete('/users/:id', UsersController.deleteUser);

export default UsersRouter