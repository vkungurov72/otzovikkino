import db from "../db/db.js"

class UsersService {
    async getAll() {
        const usersAll = await db('users')
            .select()
        return usersAll
    }

    async getById(id) {
        const user = await db('users')
            .where({ id })
            .first()
        return user
    }

    async create(userData) {
        const [id] = await db('users')
            .insert(userData)
        const newUser = await this.getUserById(id)
        return newUser
    }

    async update(id, userData) {
        await db('users')
            .where({ id })
            .update(userData)
        const updatedUser = await this.getUserById(id)
        return updatedUser
    }

    async delete(id) {
        const deletedUser = await this.getUserById(id)
        await db('users')
            .where({ id })
            .delete()
        return deletedUser
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new UsersService()