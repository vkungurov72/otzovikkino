import db from "../db/db.js"

class GenresService {

    async getAll() {
        const genresAll = await db('genres')
            .select()
        return genresAll
    }
    async getById(id) {
        const genre = await db('genres')
            .where({ id })
            .first()
        return genre
    }

    async create(genreData) {
        const newGenre = await db('genres')
            .insert(genreData)
        return newGenre
    }

    async updateById(id, genreData) {
        const updatedGenre = await db('genres')
            .where({ id })
            .update(genreData)
        return updatedGenre
    }

    async deleteById(id) {
        const deletedGenre = await db('genres')
            .where({ id })
            .del()
        return deletedGenre
    }
}

export default new GenresService()