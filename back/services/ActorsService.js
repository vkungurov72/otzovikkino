import db from "../db/db.js"

class ActorsService {
    async getAll() {
        const actorsAll = await db('actors')
            .select()
        return actorsAll
    }
    async getById(id) {
        const actor = await db('actors')
            .where({ id })
            .first()
        return actor
    }

    async create(actorData) {
        const [actorId] = await db('actors')
            .insert(actorData)
        const newActor = await this.getActorById(actorId)
        return newActor
    }

    async update(id, actorData) {
        await db('actors')
            .where({ id })
            .update(actorData)
        const updatedActor = await this.getActorById(id)
        return updatedActor
    }

    async delete(id) {
        const actorToDelete = await this.getActorById(id)
        await db('actors')
            .where({ id })
            .del()
        return actorToDelete
    }

}


export default new ActorsService()
