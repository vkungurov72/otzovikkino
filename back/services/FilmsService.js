import db from "../db/db.js"

class FilmsService {
    async getAll() {
        const filmsAll = await db('films')
            .select()
        return filmsAll
    }

    async getById(id) {
        const film = await db('films')
            .where({ id })
            .first()
        return film
    }

    async create(filmData) {
        const [id] = await db('films')
            .insert(filmData)
        const newFilm = await this.getById(id)
        return newFilm
    }

    async update(id, filmData) {
        await db('films')
            .where({ id })
            .update(filmData)
        const updatedFilm = await this.getById(id)
        return updatedFilm
    }

    async delete(id) {
        const deletedFilm = await this.getById(id)
        await db('films')
            .where({ id })
            .delete()
        return deletedFilm
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new FilmsService()