import React from 'react'
import AppRouter from './components/AppRouter/AppRouter.jsx'
import './App.css'

const App = () => {
  return (
    <div className='app'>
      <AppRouter />
    </div>
  )
}

export default App
