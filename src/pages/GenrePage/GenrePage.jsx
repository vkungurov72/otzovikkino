import React from 'react'
import classes from './GenrePage.module.css'
import ListGenres from '../../components/ListGenres/ListGenres.jsx'

const GenrePage = () => {
  return (
    <div>
      <ListGenres />
    </div>
  )
}

export default GenrePage
