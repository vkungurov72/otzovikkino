import React from 'react'
import RegistrationForm from '../../components/RegistrationForm/RegistrationForm'
import classes from './Registration.module.css'

const Registration = () => {
  return (
    <div className={classes._registrationPage}>
      <RegistrationForm />
    </div>
  )
}

export default Registration
