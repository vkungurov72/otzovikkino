import React from 'react'
import classes from './FilmPage.module.css'
import ListFilms from '../../components/ListFilms/ListFilms.jsx'

const FilmPage = () => {
  return (
    <div>
      <ListFilms />
    </div>
  )
}

export default FilmPage
