import React from 'react'
import AuthForm from '../../components/AuthForm/AuthForm'
import classes from './Auth.module.css'
const Auth = () => {
  return (
    <div className={classes._authPage}>
      <AuthForm />
    </div>
  )
}

export default Auth
