import React from 'react'

import classes from './MainPage.module.css'

const MainPage = () => {
  return <div className={classes._homePage}></div>
}

export default MainPage
