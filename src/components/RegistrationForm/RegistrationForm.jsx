import React from 'react'
import classes from './RegistrationForm.module.css'

const RegistrationForm = () => {
  const [firstName, setFirstName] = React.useState('')
  const [lastName, setLastName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [errorMessage, setErrorMessage] = React.useState('')

  const handleSubmit = event => {
    event.preventDefault()
    if (!firstName || !lastName || !email || !password) {
      setErrorMessage('Пожалуйста, заполните все поля')
    } else if (password.length < 6) {
      setErrorMessage('Пароль должен содержать не менее 6 символов')
    } else {
      console.log(firstName, lastName, email, password)
      // отправка данных на сервер
    }
  }

  return (
    <div className={classes.registrationForm}>
      <h1>Регистрация</h1>
      {errorMessage && <p className={classes.errorMessage}>{errorMessage}</p>}
      <form onSubmit={handleSubmit}>
        <div className={classes.registrationFormInputs}>
          <label htmlFor='firstName'>Имя:</label>
          <input
            type='text'
            id='firstName'
            value={firstName}
            onChange={event => setFirstName(event.target.value)}
          />
        </div>
        <div className={classes.registrationFormInputs}>
          <label htmlFor='lastName'>Фамилия:</label>
          <input
            type='text'
            id='lastName'
            value={lastName}
            onChange={event => setLastName(event.target.value)}
          />
        </div>
        <div className={classes.registrationFormInputs}>
          <label htmlFor='email'>Email:</label>
          <input
            type='email'
            id='email'
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </div>
        <div className={classes.registrationFormInputs}>
          <label htmlFor='password'>Пароль:</label>
          <input
            type='password'
            id='password'
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </div>
        <button type='submit'>Зарегистрироваться</button>
      </form>
    </div>
  )
}

export default RegistrationForm
