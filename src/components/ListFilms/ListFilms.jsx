import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Film from './Film/Film'
import classes from './ListFilms.module.css'
const ListFilms = () => {
  const [films, setFilms] = useState([])

  async function getAllFilms () {
    const { data: res } = await axios.get('/films')
    setFilms(res)
  }

  useEffect(() => {
    getAllFilms()
  }, [])

  return (
    <div className={classes._ListFilms}>
      {films.map(film => (
        <Film key={film.id} name={film.name} />
      ))}
    </div>
  )
}

export default ListFilms
