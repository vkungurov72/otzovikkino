import React from 'react'
import {
  RouterProvider,
  Route,
  createBrowserRouter,
  createRoutesFromElements
} from 'react-router-dom'
import Registration from '../../pages/Registration/Registration'
import Auth from '../../pages/Auth/Auth'
import NotFound from '../../pages/NotFound/NotFound'
import Layout from '../Layout/Layout'
import MainPage from '../../pages/MainPage/MainPage'
import FilmPage from '../../pages/FilmPage/FilmPage'
import GenrePage from '../../pages/GenrePage/GenrePage'

const AppRouter = () => {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <>
        <Route path='/' element={<Layout />}>
          <Route index element={<NotFound />}></Route>
          <Route path='*' element={<NotFound />}></Route>
          <Route path='registration' element={<Registration />} />
          <Route path='main' element={<MainPage />} />
          <Route path='film' element={<FilmPage />} />
          <Route path='auth' element={<Auth />} />
          <Route path='genre' element={<GenrePage />} />
        </Route>
      </>
    )
  )

  return <RouterProvider router={router} />
}

export default AppRouter
