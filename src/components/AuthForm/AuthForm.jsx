import React from 'react'
import classes from './AuthForm.module.css'

const AuthForm = () => {
  const [loginInfo, setLoginInfo] = React.useState('')
  const [passwordInfo, setPasswordInfo] = React.useState('')
  const [errorMessage, setErrorMessage] = React.useState('')

  const sendInfo = () => {
    if (loginInfo === '' || passwordInfo === '') {
      setErrorMessage('Заполните все поля')
    } else {
      console.log(loginInfo, passwordInfo)
    }
  }

  return (
    <div className={classes._authForm}>
      <div className={classes.authFormLeftMenu}>
        <h1>Авторизоваться</h1>
        <div className={classes.authInfo}>
          <input
            className={classes.authFormLogin}
            placeholder='Логин'
            type='email'
            onChange={e => setLoginInfo(e.target.value)}
          />
          <input
            className={classes.authFormPassw}
            placeholder='Пароль'
            type='password'
            onChange={e => setPasswordInfo(e.target.value)}
          />
        </div>
        {errorMessage && <p className={classes.errorMessage}>{errorMessage}</p>}
        <button className={classes.authFormEntr} onClick={sendInfo}>
          Войти
        </button>
      </div>
      <div className={classes.authFormRightMenu}>
        <h2>Еще не зарегистрированы?</h2>
        <p>Тогда присоединяйтесь к нам и мы подберём для Вас фильмы!</p>
        <a href='/registration'>Зарегистрироваться</a>
      </div>
    </div>
  )
}

export default AuthForm
