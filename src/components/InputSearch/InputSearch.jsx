/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import classes from './InputSearch.module.css'
import search from '../../ui/img/search.png'
const InputSearch = () => {
  return (
    <div className={classes._inputSearch}>
      <input className={classes.inputSearch} placeholder='Поиск по сайту' />
      <img src={search} className={classes.imgSearch} alt='imgSearch' />
    </div>
  )
}

export default InputSearch
