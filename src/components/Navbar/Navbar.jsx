import React from 'react'
import { Link } from 'react-router-dom'
import classes from './Navbar.module.css'
import InputSearch from '../InputSearch/InputSearch'
import logo from '../../ui/img/logo.svg'
const Navbar = () => {
  return (
    <header className={classes.header}>
      <img src={logo} className={classes.logo} alt='logo' />
      <Link to='/main'>Главная</Link>
      <Link to='/film'>Фильмы</Link>
      <Link to='/genre'>Жанры</Link>
      <Link>Рейтинг топ 250 фильмов</Link>
      <Link>Актеры</Link>
      <Link>Новости</Link>
      <Link to='/auth'>Авторизация</Link>
      <Link to='/registration'>Регистрация</Link>
      <div>
        <InputSearch />
      </div>
    </header>
  )
}

export default Navbar
