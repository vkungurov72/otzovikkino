import React, { useEffect, useState } from 'react'
import axios from 'axios'
import classes from './ListGenres.module.css'
import Genre from './Genre/Genre'
const ListGenres = () => {
  const [genres, setGenres] = useState([])

  async function getAllGenres () {
    const { data: res } = await axios.get('/genres')
    setGenres(res)
  }

  useEffect(() => {
    getAllGenres()
  }, [])

  return (
    <div className={classes._ListGenres}>
      {genres.map(genre => (
        <Genre key={genre.id} name={genre.name} />
      ))}
    </div>
  )
}

export default ListGenres
