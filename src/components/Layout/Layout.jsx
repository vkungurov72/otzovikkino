import React from 'react'
import { Outlet } from 'react-router-dom'
import Navbar from '../Navbar/Navbar'
import classes from './Layout.module.css'
const Layout = () => {
  return (
    <div className={classes.layout}>
      <Navbar />
      <Outlet />
      <footer className={classes.footer}>Зарегистрировано в 2023 году</footer>
    </div>
  )
}

export default Layout
