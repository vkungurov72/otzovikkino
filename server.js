import FilmsRouter from "./back/routes/FilmsRouter.js";
import expess from "express";
import GenresRouter from "./back/routes/GenresRouter.js";
import ActorsRouter from "./back/routes/ActorsRouter.js";
import UsersRouter from "./back/routes/UsersRouter.js";

const app = expess();
app.use(expess.json());
app.use('/', FilmsRouter)
app.use('/', GenresRouter)
app.use('/', ActorsRouter)
app.use('/', UsersRouter)

app.listen(8080, () => console.log('server'))
